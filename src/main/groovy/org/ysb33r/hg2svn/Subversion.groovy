// ============================================================================
// (C) Copyright Schalk W. Cronjé 2014
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// Full license text available at https://www.gnu.org/licenses/gpl-2.0.txt
//
// ============================================================================
package org.ysb33r.hg2svn

import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import groovy.transform.PackageScope
import groovy.util.logging.Slf4j
import org.tmatesoft.svn.core.SVNDepth
import org.tmatesoft.svn.core.SVNException
import org.tmatesoft.svn.core.SVNPropertyValue
import org.tmatesoft.svn.core.wc.ISVNOptions
import org.tmatesoft.svn.core.wc.ISVNPropertyHandler
import org.tmatesoft.svn.core.wc.ISVNStatusHandler
import org.tmatesoft.svn.core.wc.SVNClientManager
import org.tmatesoft.svn.core.wc.SVNCopySource
import org.tmatesoft.svn.core.wc.SVNRevision
import org.tmatesoft.svn.core.wc.SVNStatus

import java.nio.file.Files
import java.nio.file.LinkOption
import java.nio.file.Path

/**
 * @author Schalk W. Cronjé.
 */
@CompileStatic
@Slf4j
class Subversion {
    File svnRoot
    Path svnRootPath

    @PackageScope SVNClientManager svn

    @CompileDynamic
    Subversion( properties=[:] ) {
        svnRoot=new File(properties.svnRoot)
        svnRootPath=svnRoot.toPath()

        ISVNOptions myOptions
        svn = SVNClientManager.newInstance(myOptions,properties.username,properties.password)

    }

    File branchDir(final String branchName) {
        final String child = branchName=='default' ? 'trunk' : "branches/${branchName}"
        new File( svnRoot,child )
    }

    def cleanup() {
        log.info "Running a cleanup on ${svnRoot}"
        svn.getWCClient().doCleanup(svnRoot,true)
    }

    def revert() {
        log.info "Reverting changes on ${svnRoot}"
        svn.getWCClient().doRevert([svnRoot] as File[],SVNDepth.INFINITY,null)
    }

    def remove(final File file) {
        if(file.exists()) {
            def status = getStatus(file)
            if(status==null || !status.isVersioned()) {
                log.info "Removing ${file} (unversioned)"
                if(file.isDirectory()) {
                    file.deleteDir()
                } else {
                    file.delete()
                }
            } else {
                log.info "Removing ${file}"
                svn.getWCClient().doDelete(file, true, false)
            }
        } else {
            log.info "Request to delete non-existing ${file} - ignored."
        }
    }

    def propset(final File file,final String propName,final String value) {
        if(file.exists()) {
            log.info "Setting ${propName}=${value} on ${file}"
            svn.getWCClient().doSetProperty(
                    file,
                    propName,
                    SVNPropertyValue.create(value),
                    true,
                    SVNDepth.EMPTY,
                    ISVNPropertyHandler.NULL,
                    null
            )
        } else {
            log.info "Request to set ${propName}=${value} on non-existing ${file} - ignored."
        }
    }

    def propdel(final File file,final String propName) {
        if(file.exists()) {
            log.info "Removing '${propName}' from ${file}"
            svn.getWCClient().doSetProperty(
                    file,
                    propName,
                    null,
                    true,
                    SVNDepth.EMPTY,
                    ISVNPropertyHandler.NULL,
                    null
            )
        }
    }

    def update() {
        svn.updateClient.doUpdate(svnRoot,SVNRevision.HEAD,SVNDepth.INFINITY,true,false)
    }

    def mkdir(final File dir) {
        log.info "Mkdir ${dir}"
        svn.getWCClient().doAdd([dir] as File[],true,true,false,SVNDepth.EMPTY,false,false,true)
    }

    def copy(final File src,final File dest) {
        log.info "Copy ${src} -> ${dest}"
        svn.copyClient.doCopy(
                [new SVNCopySource(SVNRevision.WORKING,SVNRevision.WORKING,src)] as SVNCopySource[],
                dest,false,true,false
        )
    }

    def commit(final String msg) {
        log.info "Committing.. [${msg}]"

        svn.commitClient.doCommit([svnRoot] as File[],false,msg,false,true)
    }

    def commit(final String msg,final File file) {
        log.info "Committing.. [${msg}]"

        svn.commitClient.doCommit([file] as File[],false,msg,false,true)
    }

    def addFiles( File... files ) {
        files.each { File it ->
            if(it.exists()) {
                log.info "Adding file ${it}"
                svn.getWCClient().doAdd([it] as File[],true,false,false,SVNDepth.EMPTY,false,false,true)
            } else {
                log.error "Request to add non-existing file ${it} - ignored."
            }

        }
    }

    def addFiles( Path... files ) {
        files.each { Path it ->
            if(Files.exists(it,LinkOption.NOFOLLOW_LINKS)) {
                if(Files.isSymbolicLink(it)) {
                    log.info "Attempting to add symlink by recursion from ${it.parent}"
                    svn.getWCClient().doAdd(it.parent.toFile(), true, false, false, SVNDepth.IMMEDIATES, false, true)
                } else {
                    log.info "Adding file ${it}"
                    svn.getWCClient().doAdd(it.toFile(), true, false, false, SVNDepth.EMPTY, false, true)
                }
            } else {
                log.error "Request to add non-existing file ${it} - ignored."
            }

        }
    }


    /** Cleans out unversioned files (but not ones in the ignored list)
     *
     * @param topDir
     * @return
     */
    def deleteUnversioned( final File topDir ) {

        List<File> deleteables = []

        def deleter = [
            handleStatus : { SVNStatus status ->
                if(!status.isVersioned()) {
                    deleteables << status.file
                }
            }
        ] as ISVNStatusHandler

        svn.statusClient.doStatus(
                topDir,
                SVNRevision.WORKING,
                SVNDepth.INFINITY,
                false,
                false,
                false,
                false,
                deleter,
                null
        )

        deleteables.each { File target->
            if(target.exists()) {
                log.info "Deleting unversioned - ${target}"
                if(target.isDirectory()) {
                    target.deleteDir()
                } else {
                    target.delete()
                }
            }
        }
    }

    /** Get status of single file
     *
     * @param file
     * @return Status of file, or null if file is not versioned
     */
    SVNStatus getStatus(File file) {
        try {
            svn.getStatusClient().doStatus(file, false)
        } catch(SVNException e) {
            if(e.message.contains('is not a working copy')) {
                return null
            }
            throw e
        }
    }
}
