// ============================================================================
// (C) Copyright Schalk W. Cronjé 2014
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// Full license text available at https://www.gnu.org/licenses/gpl-2.0.txt
//
// ============================================================================
package org.ysb33r.hg2svn

import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import groovy.transform.PackageScope
import groovy.util.logging.Slf4j
import groovy.util.CliBuilder
import org.tmatesoft.hg.core.HgChangeset
import org.tmatesoft.hg.core.HgFileRevision
import org.tmatesoft.hg.core.HgRepoFacade
import org.tmatesoft.hg.core.Nodeid
import org.tmatesoft.hg.repo.HgManifest
import org.tmatesoft.hg.repo.HgRepository

import java.nio.channels.Channels
import java.nio.channels.FileChannel
import java.nio.file.Files
import java.nio.file.LinkOption
import java.nio.file.Path
import java.nio.file.Paths
import java.util.List

import static java.nio.file.StandardOpenOption.CREATE
import static java.nio.file.StandardOpenOption.DSYNC
import static java.nio.file.StandardOpenOption.TRUNCATE_EXISTING
import static java.nio.file.StandardOpenOption.WRITE

/**
 * @author Schalk W. Cronjé.
 */
@Slf4j
@CompileStatic
class Hg2Svn {
    HgRepoFacade hgRepo
    Subversion svn
    boolean needRevert = false
    boolean needCleanup = false
    boolean needPristine = false

    @PackageScope Integer restartFrom
    @PackageScope def commandLine
    @PackageScope File trackerFile

    @CompileDynamic
    private Hg2Svn(def commandline) {
        this.commandLine = commandline

        hgRepo = new HgRepoFacade()
        assert hgRepo.initFrom(new File(commandLine.hg))

        svn = new Subversion(username : commandLine.svnuser , password : commandLine.svnpass, svnRoot : commandLine.svn)
        trackerFile = new File(svn.svnRoot,'.svn/hg2svn')

        if(commandLine.restart) {
            restartFrom = commandLine.restart.toInteger()
        }

        if(commandLine.'restart-lastgood') {
            if(trackerFile.exists()) {
                Integer tmp=trackerFile.text.toInteger() + 1
                restartFrom = (restartFrom == null) ? tmp : [tmp,restartFrom].min()
            }
        }

        if(commandLine.cleanup) {
            needCleanup = true
        }

        if(commandLine.revert) {
            needRevert = true
        }
        if(commandLine.pristine) {
            needPristine = true
        }

    }

    def run() {
        if(needCleanup) {
            svn.cleanup()
        }
        if(needRevert) {
            svn.revert()
        }
        if(needPristine) {
            svn.deleteUnversioned(svn.svnRoot)
        }
        processLogFromGroundZero()
    }

    @PackageScope
    String commitMsg(HgChangeset hgRev) {
            """${hgRev.comment}

Author: ${hgRev.user}
Date: ${hgRev.date}
Mercurial index: ${hgRev.revisionIndex}
Mercurial id: ${hgRev.nodeid}
"""
    }

    @PackageScope
    void processLogFromGroundZero() {
        def log = hgRepo.createLogCommand()
        if(restartFrom) {
            log.range(restartFrom,-3)
        }
        log.execute().each { HgChangeset hgRev ->
            info "Revision: ${hgRev.revisionIndex} ${hgRev.firstParentRevision}:${hgRev.secondParentRevision}"
            checkForBranchCreation hgRev
            Path svnBase = svn.branchDir(hgRev.branch).toPath()

            List<HgFileRevision> mods = removeDotSvnFromRevisions(hgRev.modifiedFiles)
            List<HgFileRevision> adds = []

            removeDotSvnFromRevisions(hgRev.addedFiles).each { HgFileRevision hgr ->
                if(!mods.find { HgFileRevision needle ->
                    needle.path == hgr.path
                }) {
                    adds+= hgr
                }

            }

            modifyFiles svnBase,mods
            addFiles  svnBase, adds
            deleteFiles svnBase,removeDotSvnFromPath(hgRev.removedFiles)
            svn.commit commitMsg(hgRev)
            trackerFile.text = "${hgRev.revisionIndex}"
        }
    }

    def checkForBranchCreation(HgChangeset changeset) {
        if(changeset.firstParentRevision != Nodeid.NULL && !changeset.isMerge()) {
            def parent=hgRepo.createLogCommand().changeset(changeset.firstParentRevision).execute()[0]
            if(parent.branch != changeset.branch) {
                info "Found a branch '${changeset.branch}'"
                File svnBase = svn.branchDir changeset.branch
                File svnParentBase = svn.branchDir parent.branch
                if(!svnBase.exists()) {
                    final String message = "Created branch ${changeset.branch}"
                    svn.with {
                        cleanup()
                        update()
                        copy svnParentBase,svnBase
                        commit message
                    }
                }
            }
        }
    }


    @PackageScope
    def addFiles(Path svnBase,List<HgFileRevision> files) {
        Path target
        File targetFile
        HgRepository repository = hgRepo.repository

        files.each { HgFileRevision fr ->
            target= svnBase.resolve(fr.path.toString())
            targetFile = target.toFile()

            if(!targetFile.parentFile.exists()) {
                svn.mkdir targetFile.parentFile
            }

            def fileType = fr.fileFlags
            if (fileType == HgManifest.Flags.Link) {
                info "Adding ${targetFile} as symbolic link"
                hgSymlinkToSvn (fr, target)
                assert Files.exists(target,LinkOption.NOFOLLOW_LINKS)

            } else {
                hgContentToSvn fr,target
                if(fileType == HgManifest.Flags.Exec) {
                    info "Adding ${targetFile} as executable"
                } else {
                    info "Adding ${targetFile}"
                }

            }
            svn.addFiles target
            if(fileType == HgManifest.Flags.Exec) {
                svn.propset(targetFile,'svn:executable','1')
            }
        }
    }

    @PackageScope
    def modifyFiles(Path svnBase, List<HgFileRevision> files ) {
        Path target
        File targetFile
        files.each { HgFileRevision fr ->
            target= svnBase.resolve(fr.path.toString())
            targetFile = target.toFile()
            if(!targetFile.parentFile.exists()) {
                svn.mkdir targetFile.parentFile
            }
            def fileType = fr.fileFlags
            if (fileType == HgManifest.Flags.Link && Files.isSymbolicLink(target)) {
                info "Modifying ${targetFile} (Symbolic link change)"
                target= hgSymlinkToSvn (fr, target)
            } else if (fileType == HgManifest.Flags.Link && !Files.isSymbolicLink(target)) {
                info "Mercurial says ${targetFile} should be a symbolic link. Will replace with symlink."
                svn.propdel targetFile,'svn:special'
                svn.remove targetFile
                svn.commit "Intermediate commit deleting ${targetFile} in order to replace with symlink in subsequent commit."
                hgSymlinkToSvn (fr, target)
                assert Files.exists(target,LinkOption.NOFOLLOW_LINKS)
                svn.addFiles target
            } else if(fileType != HgManifest.Flags.Link && Files.isSymbolicLink(target)) {
                info "${targetFile} is already a symbolic link."
                Path pointee=target.resolveSibling(Files.readSymbolicLink(target))
                info "Modifying ${pointee} instead"
                    hgContentToSvn fr,pointee
            } else {
                info "Modifying ${targetFile}"
                hgContentToSvn fr,target
            }
        }
    }

    @PackageScope
    def deleteFiles(Path svnBase, List<org.tmatesoft.hg.util.Path> files ) {
        Path target
        File targetFile
        files.each { org.tmatesoft.hg.util.Path path ->
            target= svnBase.resolve(path.toString())
            targetFile = target.toFile()
            info "Deleting ${targetFile}"
            svn.remove targetFile
        }
    }


    @PackageScope
    def hgContentToSvn(HgFileRevision fr,Path target) {
        def output = FileChannel.open( target,CREATE,WRITE,TRUNCATE_EXISTING,DSYNC )
        try {
            fr.putContentTo(
                [ write : { java.nio.ByteBuffer buffer ->
                    output.write(buffer)
                    }
                ] as org.tmatesoft.hg.util.ByteChannel
            )
        }
        finally {
            output.close()
        }
    }

    @PackageScope
    Path getPointeeFromRawData(HgFileRevision fr) {
        def raw=hgRepo.repository.getFileNode(fr.path)
        def index=raw.getRevisionIndex(fr.revision)
        def os = new ByteArrayOutputStream()
        def output= Channels.newChannel(os)
        raw.content(
                index,
                [ write : { java.nio.ByteBuffer buffer ->
                    output.write(buffer)
                }
                ] as org.tmatesoft.hg.util.ByteChannel
        )

        Path pointee = new File(os.toString()).toPath()
        output.close()
        return pointee
    }

    @PackageScope
    Path hgSymlinkToSvn(HgFileRevision fr,Path target) {
        Path pointee = getPointeeFromRawData(fr)
        if(Files.exists(target,LinkOption.NOFOLLOW_LINKS)) {
            log.info "Updating symbolic link ${target} -> ${pointee}"
            Files.delete(target)
        } else {
            log.info "Creating symbolic link ${target} -> ${pointee}"
        }
        Files.createSymbolicLink (target,pointee)
    }

    @CompileDynamic
    private void info(final String msg) {
        log.info msg
    }

    @PackageScope
    List<HgFileRevision> removeDotSvnFromRevisions(List<HgFileRevision> files) {
        files.findAll { HgFileRevision hfr ->
            !hfr.path.segments().any { it == '.svn'}
        } as List<HgFileRevision>
    }

    @PackageScope
    List<org.tmatesoft.hg.util.Path> removeDotSvnFromPath(List<org.tmatesoft.hg.util.Path> files) {
        files.findAll { org.tmatesoft.hg.util.Path hfr ->
            !hfr.segments().any { it == '.svn'}
        } as List<org.tmatesoft.hg.util.Path>
    }

    @CompileDynamic
    static def parseCommandLine(def cmdArgs) {
        CliBuilder cli = new CliBuilder(
                usage: 'hg2svn [options]',
                header: 'Provides a one way conversion from Mercurial to Subversion'
        )

        cli.with {
            _ longOpt:'help',     required: false, 'Displays this help'
            _ longOpt:'svn',      argName:'DIR', args:1, required: true, 'Path to local Subversion repository'
            _ longOpt:'hg',       argName:'DIR', args:1, required: true, 'Path to local Mercurial repository'
            _ longOpt:'svnuser',  argName:'USERNAME', args:1, required: true, 'Subversion username'
            _ longOpt:'svnpass',  argName:'PASSWORD', args:1, required: true, 'Subversion password'
            _ longOpt:'restart',  argName: 'HGREVINDEX', args:1, required:false, 'Restart from Mercurial Revision Index'
            _ longOpt:'restart-lastgood', required: false, "Restart from last good Mercurcial Revision Index"
            _ longOpt:'revert',   required:false,  'Run a revert before starting updates'
            _ longOpt:'cleanup',  required:false, 'Run a cleanup before starting updates'
            _ longOpt:'pristine', required:false, 'Removes all unversioned files'
        }

        def options= cli.parse(cmdArgs)

        if(!options || options.help) {
            cli.usage()
            return null
        }

        options
    }

    static void main(String[] args) {
        def commandLine = parseCommandLine(args)
        if (!commandLine) {
            System.exit(1)
        }

        new Hg2Svn(commandLine).run()
        System.exit(0)
    }
}
