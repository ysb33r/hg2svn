// ============================================================================
// (C) Copyright Schalk W. Cronjé 2014
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// Full license text available at https://www.gnu.org/licenses/gpl-2.0.txt
//
// ============================================================================
package org.ysb33r.hg2svn

import spock.lang.Specification

/**
 * @author Schalk W. Cronjé.
 */
class SvnStatusSpec extends Specification {

    final static File TESTROOT = new File( 'build/tmp/test-repos' )
    final static File TESTARCHIVEDIR = new File( 'build/resources/test/data').absoluteFile
    final static File SVNROOT = new File(TESTROOT,'testsvn')

    def svn

    void setup() {
        if(TESTROOT.exists()) {
            TESTROOT.deleteDir()
        }
        TESTROOT.mkdirs()
        UnpackRepositories.go(TESTARCHIVEDIR,TESTROOT)

        svn = new Subversion(svnRoot: SVNROOT.absolutePath, username : 'u', password : 'p')
    }

    def "When checking status for existing file, with parent not a working dir, should not throw exception"() {

        given:
            File parent= new File(SVNROOT,'trunk')
            File target= new File(parent,'target.txt')
            parent.mkdirs()
            target.text='test'

        when:
            def status = svn.getStatus(target)

        then:
            status == null
    }

    def "Deleting a local file with parent not a working dir, should not throw exception"() {

        given:
            File parent= new File(SVNROOT,'trunk')
            File target= new File(parent,'target.txt')
            parent.mkdirs()
            target.text='test'

        when:
            svn.remove(target)

        then:
            !target.exists()
    }
}
