package org.ysb33r.hg2svn

import org.ysb33r.groovy.dsl.vfs.VFS

/**
 * @author Schalk W. Cronjé.
 */
class UnpackRepositories {
    static void go( final File srcDir, final File targetDir ) {
        def vfs = new VFS()
        vfs {
            [ 'empty-svn-repo.tar','test-hg-repo.tar' ].each {
                cp "tar://${srcDir.absoluteFile}/${it}", targetDir, recursive : true
            }
        }
    }
}
