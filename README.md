# Mercurial to Subversion Conversion Helper Script

This is a utility to aid the migration from Mercurial to Subversion repositories. This is not something that happens
a lot (thankfully), but where the need arises this script should help quite a bit.


## Features

- Can import a Mercurial repository from revision 0 all the way through to latest revision with full history.
- Can restart an import from any Mercurial revision ID.
- Symlinks and conversions between symlinks & regular files are handled in most cases (see Known Issues).
- All Mercurial branches will be re-created in Subversion via a `svn copy` command.
- Any `.svn` folders found in mercurial repository are ignored.

## Known Issues

- Tags are not converted 
- Sometimes symlinks will end up as a copied file in Subversion. This is typical when a file is converted between a 
symlink and a regular file more than once.
- Directories cannot be converted into symlinks. (Curretnly needs a manual fix + restart).
- Deleted Mercurial branches are not deleted in Subversion.
- `.hgignore` files are committed to Subversion instead of updating `svn:ignore` property

If you come across a `handshake alert: unrecognized_name` error then add `-Djsse.enableSNIExtension=false` to `JAVA_OPTS`.
(see http://forum.portswigger.net/thread/518/issue-handshake-alert-unrecognized-name).

## Building

```bash
git clone https://ysb33r@bitbucket.org/ysb33r/hg2svn.git
./gradlew distZip

```

The completed build will be found in `hg2svn/build/distributions`

## Requirements

`hg2svn` requires JDK 7 for building and running.

## Running

```bin/hg2svn --help```

